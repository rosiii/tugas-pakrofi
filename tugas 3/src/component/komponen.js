import React, {Component} from 'react';
import {View,StyleSheet,ScrollView,Image,} from 'react-native';

class FlexBox extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.rosy1}>
                    <Image source={require('../aset/a.jpg')} style={{width:150,height:50}}/>
                    <Image source={require('../aset/b.jpg')} style={{width:100,height:50,marginLeft:100}}/>
                </View>
                <View style={styles.rosy2}>
                    <ScrollView>
                        <ScrollView horizontal>
                        <Image source={require('../aset/c.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/c.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/c.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/c.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/c.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/c.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/c.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        </ScrollView>
                        <Image source={require('../aset/d.jpg')} style={{width:350,height:570}}/>
                        <Image source={require('../aset/d.jpg')} style={{width:350,height:570}}/>
                        <Image source={require('../aset/d.jpg')} style={{width:350,height:570}}/>
                        <Image source={require('../aset/d.jpg')} style={{width:350,height:570}}/>
                        <Image source={require('../aset/d.jpg')} style={{width:350,height:570}}/>
                    </ScrollView>
                </View>
                <View style={styles.rosy3}>
                    <Image source={require('../aset/e.jpg')} style={{width:40,height:40}}/>
                    <Image source={require('../aset/f.jpg')} style={{width:40,height:40}}/>
                    <Image source={require('../aset/g.jpg')} style={{width:40,height:40}}/>
                    <Image source={require('../aset/h.jpg')} style={{width:40,height:40}}/>
                    <Image source={require('../aset/i.jpg')} style={{width:40,height:40}}/>
                </View>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1
    },
    rosy1:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'row'
    },
    rosy2:{
        flex:9,
        backgroundColor:'white'
    },
    rosy3:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'row',
        justifyContent:'space-around'
    },
});
export default FlexBox;